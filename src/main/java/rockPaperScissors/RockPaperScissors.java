package rockPaperScissors;

import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */  
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    
    public void run() {
        
        //computer moove randome
        String[] rps = {"rock", "paper", "scissors"};
        String computerMove = rps[new Random().nextInt(rps.length)];
        String playerMove = "";
        String win = "Human wins!";
        String lose = "Computer wins!";
        
        //roundcounter
        System.out.println("Let's play round " + roundCounter);

        // your input moove
        while (true) {
            String tmp = readInput("Your choice (Rock/Paper/Scissors)?");
            tmp.toLowerCase();
            if (tmp.equals("rock") || tmp.equals("paper") || tmp.equals("scissors")) {
                playerMove = tmp;
                break;
            }
            
            else {System.out.println("I do not understand " + tmp +". Could you try again?");
            }
        }
       
        String tmp2 = ("Human chose " + playerMove + ", computer chose " + computerMove +". ");
        //tie
        if (playerMove.equals(computerMove)){
            System.out.println(tmp2 + "It's a tie!");
        }
        //who wins
        else if (playerMove.equals("rock")) {
            if (computerMove.equals("paper")) {
                System.out.println(tmp2 + lose);
                computerScore += 1;
            }
            else if (computerMove.equals("scissors")) {
                System.out.println(tmp2 + win);
                humanScore += 1;
            }
        }
        else if (playerMove.equals("paper")) {
            if (computerMove.equals("rock")) {
                System.out.println(tmp2 + win);
                humanScore += 1;
            }
            else if (computerMove.equals("scissors")) {
                System.out.println(tmp2 + lose);
                computerScore += 1;
                }
        }

        else if (playerMove.equals("scissors")) {
            if (computerMove.equals("rock")) {
                System.out.println(tmp2 + lose);
                computerScore += 1;
            }
            else if (computerMove.equals("paper")) {
                System.out.println(tmp2 + win);
                humanScore += 1;
                }
        }

        //score output
        System.out.println("Score: human " + humanScore + ", computer " + computerScore );

        // do you want to play again
        
        String newround = readInput("Do you wish to continue playing? (y/n)?");

        if (!newround.equals("y")){
            System.out.println("Bye bye :)");
              
        }
        else {
            roundCounter += 1;
            run();
        }
        
    }

    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
}

